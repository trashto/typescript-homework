const API_KEY = 'b24ae3a6da8aecdf85d57234690dee83';

interface SearchParams {
  [key: string]: string | number
}

class Client {
  
  get(url: string, queryParams?: SearchParams) {
    const queryString: string = this.createQueryString(queryParams);
    return fetch(`${url}?api_key=${API_KEY}${queryString}`)
      .then(response => response.json())
      .catch(error => console.error('Error ' + error))
  }

  createQueryString(queryParams?: SearchParams) {
    if (!queryParams) {
      return '';
    }

    let stringParams: string[] = [];
    for (let key in queryParams) {
      stringParams.push(`${key}=${queryParams[key]}`)
    }
    return `&${stringParams.join('&')}`
  }
}

export default new Client


