const ids: string[] = [];

function handleLikes(event: any): void {
  const heart = event.target;
  const card = heart.closest('div.card');

  if (event.target && heart.tagName === 'path') {
    toggleHeart(heart.closest('svg'));
    pushIdToLocalStorage(card.id)
  }

}

function toggleHeart(heart: SVGSVGElement): void {
  const heartColor = heart.getAttribute('fill');
  if (heartColor === '#ff000078') {
    heart.setAttribute('fill', 'red');
  } else {
    heart.setAttribute('fill', '#ff000078');
  }
}

function pushIdToLocalStorage(id: string): void {
  if (!ids.includes(id)) {
    ids.push(id)
  } else {
    ids.splice(ids.indexOf(id), 1)
  }
  localStorage.likes = ids;
}

export default handleLikes