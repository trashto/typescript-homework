import {cardValues, createMovieCardElement } from "./domHelper";



function mapMovieData(dataMovies: [], typeOfCard: string): string[] {

  return dataMovies.map((dataMovie: cardValues) => createMovieCardElement(typeOfCard, dataMovie))
} 

export default mapMovieData