import Client from "./client"
import searchFilmByName from "./searchService";
import apiUrls from "./apiUrls";
import handleLoadMore from "./loadMore"
import mapMovieData from "./mapMovieData"
import handleChange from "./handleChange"
import { randomMovieData, showRandomMovie } from "./randomMovie"
import handleLikes from "./handleLikes";
import { typeOfCard } from "./domHelper";

const filmContainer = <HTMLDivElement>document.getElementById('film-container');
const groupFilters = <HTMLDivElement>document.getElementById('button-wrapper');
const btnLoadMore = <HTMLButtonElement>document.getElementById('load-more');
const btnSearch = <HTMLButtonElement>document.getElementById('submit');

export async function render(): Promise<void> {
  // TODO render your app here
  Client.get(apiUrls.popular)
    .then(data => {
      const films: string = mapMovieData(data.results, typeOfCard.default).join('');
      const randomData = randomMovieData(data.results);
      console.log(films)
      showRandomMovie(randomData)

      filmContainer.innerHTML = films;
      localStorage.clickcount = 1;
    });

  filmContainer.addEventListener('click', handleLikes)
  Array.from(groupFilters.children)
    .forEach((element) => element.addEventListener('change', handleChange));

  btnSearch.addEventListener('click', searchFilmByName);
  btnLoadMore.addEventListener('click', handleLoadMore);
}




