import Client from "./client"
import clickCounter from "./counter"
import apiUrls from "./apiUrls"
import mapMovieData from "./mapMovieData"
import {typeOfCard} from "./domHelper"

const filmContainer = <HTMLDivElement>document.getElementById('film-container');

const popular = <HTMLInputElement>document.getElementById('popular');
const upcoming = <HTMLInputElement>document.getElementById('upcoming');
const topRated = <HTMLInputElement>document.getElementById('top_rated');


function handleLoadMore():void {
  clickCounter();
  if (popular.checked) {
    loadMore(apiUrls.popular);
  } else if (upcoming.checked) {
    loadMore(apiUrls.upcoming);
  } else if (topRated.checked) {
    loadMore(apiUrls.top_rated);
  }
}

function loadMore(path: string): void {
  Client.get(path, { page: localStorage.clickcount })
    .then(data => {
      const films: string = mapMovieData(data.results, typeOfCard.default).join('');;
      filmContainer.innerHTML += films;
    })
}
export default handleLoadMore