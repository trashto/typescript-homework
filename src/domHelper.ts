export interface cardValues {
  poster_path: string | null,
  overview: string,
  release_date: string,
  id: string
}
interface htmlParams {
  tagName: string,
  className?: string,
  attributes?: { [key: string]: string }
}

const typeOfCard = {
  default: 'col-lg-3 col-md-4 col-12 p-2',
  favorite: 'col-12 p-2'
}

function createElement({
  tagName, className, attributes = {}
}: htmlParams) {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}

function createImageElement(url: string) {
  return createElement({
    tagName: 'img',
    attributes: {
      src: url
    }
  })
}


function createMovieCardElement(containerClassNames: string, movieInfo: cardValues) {
  const container = createElement({
    tagName: 'dev',
    className: containerClassNames
  });
  const card = createElement({
    tagName: 'dev',
    className: 'card shadow-sm',
    attributes: {
      id: movieInfo.id
    }
  });

  const img = createImageElement(`https://image.tmdb.org/t/p/original//${movieInfo.poster_path}`);

  const svgText = `
  <svg
    xmlns="http://www.w3.org/2000/svg"
    stroke="red"
    fill="#ff000078"
    width="50"
    height="50"
    class="bi bi-heart-fill position-absolute p-2"
    viewBox="0 -2 18 22"
  >
    <path
        fill-rule="evenodd"
        d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
    />
</svg>
`

  const cardBody = createElement({
    tagName: 'div',
    className: 'card-body'
  });
  const cardText = createElement({
    tagName: 'p',
    className: 'card-text truncate'
  });
  cardText.innerText = movieInfo.overview;
  const dateBlock = createElement({
    tagName: 'div',
    className: 'd-flex justify-content-between align-items-center'
  });
  const dateText = createElement({
    tagName: 'small',
    className: 'text-muted'
  });
  dateText.innerText = movieInfo.release_date;

  card.innerHTML = svgText;
  card.append(img, cardBody);
  cardBody.append(cardText, dateBlock);
  dateBlock.append(dateText);

  container.append(card);

  return container.outerHTML;
}

export {
  createElement,
  createImageElement,
  createMovieCardElement,
  typeOfCard
}