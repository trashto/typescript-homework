interface apiParams {
  [key: string]: string
  search: string,
  popular: string,
  top_rated: string,
  upcoming: string
}

const apiUrls:apiParams = {
  search: 'https://api.themoviedb.org/3/search/movie',
  popular: 'https://api.themoviedb.org/3/movie/popular',
  top_rated: 'https://api.themoviedb.org/3/movie/top_rated',
  upcoming: 'https://api.themoviedb.org/3/movie/upcoming'
}

export default apiUrls

