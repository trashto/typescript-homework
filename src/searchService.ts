import Client from "./client";
import apiUrls from "./apiUrls";
import mapMovieData from "./mapMovieData"
import {typeOfCard} from "./domHelper"
// <HTMLInputElement>

function searchFilmByName(): void {
  const searchInput = <HTMLInputElement>document.getElementById('search');
  const filmContainer = <HTMLDivElement>document.getElementById('film-container');
  const searchValue = searchInput.value;

  Client.get(apiUrls.search, { query: searchValue, page: localStorage.clickcount })
    .then(data => {
      const films: string = mapMovieData(data.results, typeOfCard.default).join('');
      filmContainer.innerHTML = films;
    })


}



export default searchFilmByName