import Client from "./client"
import apiUrls from "./apiUrls"
import mapMovieData from "./mapMovieData"
import {typeOfCard} from "./domHelper"

const filmContainer = <HTMLDivElement>document.getElementById('film-container');

function handleChange(event: any): void {
  localStorage.clickcount = 1;
  const targetId: string = event.target.id;
  Client.get(apiUrls[targetId])
    .then(data => {
      const films: string = mapMovieData(data.results, typeOfCard.default).join('');
      filmContainer.innerHTML = films;
    })
}

export default handleChange