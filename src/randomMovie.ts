const movieSection = <HTMLElement>document.getElementById('random-movie')
const movieName = <HTMLHeadingElement>document.getElementById('random-movie-name')
const movieDescription = <HTMLParagraphElement>document.getElementById('random-movie-description')

interface filmParams {
  original_title: string,
  backdrop_path: string,
  overview: string,
}

function randomMovieData(data: []){
  const randomNum: number = Math.floor(Math.random() * 20);
  
  return data[randomNum]
}

function showRandomMovie(data: filmParams):void{
  movieSection.style.background = `no-repeat url(https://image.tmdb.org/t/p/original//${data.backdrop_path})`;
  movieSection.style.backgroundSize = 'cover';

  movieName.innerText = data.original_title;
  movieDescription.innerText = data.overview;

}

export {
  randomMovieData,
  showRandomMovie
}